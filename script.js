const inputNome = document.querySelector('.nome')
const inputCognome = document.querySelector('.cognome')
const inputTelefono = document.querySelector('.telefono')
const inputMatricola = document.querySelector('.matricola')
const inputEmail = document.querySelector('.email')
const select = document.querySelector('#select')
const option = document.querySelector('.option')
const btnReset = document.querySelector('.reset')
const btnSubmit = document.querySelector('.submit')
const textarea = document.querySelector('.textarea')

const regioni = ['Abruzzo', 'Basilicata', 'Calabria', 'Campania', 'Emilia-Romagna', 'Friuli-venezia-Giulia', 'Lazio', 'Liguria', 'Lombardia', 'Marche', 'Molise', 'Piemonte', 'Puglia', 'Sardegna', 'Sicilia', 'toscana', 'Trentino-Alto-Adige', 'Umbria', "Valle d'Aosta", 'Veneto']

// Funzione per popolare l'option
function insertRegion(array) {
    array.forEach(element => {

        let option = document.createElement('option')
        option.innerHTML = element

        select.appendChild(option)
    });


}

// Evento del bottone reset

btnReset.addEventListener('click', () => {
    inputNome.value = ''
    inputCognome.value = ''
    inputEmail.value = ''
    inputMatricola.value = ''
    inputTelefono.value = ''
    textarea.value = ''

})

// evento del bottone invio

btnSubmit.addEventListener('click', () => {
    if (inputNome.value == '') {
        alert('Il campo nome è obbligatorio')
    } else if (inputCognome.value == '') {
        alert('Il campo cognome è obbligatorio')
    } else if (inputMatricola.value == '') {
        alert('Il campo matricola è obbligatorio')
    } else if (inputNome.value && inputCognome.value && inputMatricola.value != '') {

        alert('Hai inviato i tuoi dati con successo')
    }

})


insertRegion(regioni)






